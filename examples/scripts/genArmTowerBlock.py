import sys
import math

def GenATB(p, a, o):
    pow2 = int(math.ceil(math.log(p,2))**2)

    f = open("ArmTowerBlock-" + str(p) + "-" + str(a) + "-" + str(o) + ".ispl","w");
    
    f.write("-- encoding example \"ArmTowerBlock\" from Clark's presentation (Teleo-Reactive)\n")
    f.write("--   with " + str(p) + " positions \n") 
    f.write("--   with " + str(a) + " agent(s) and \n") 
    f.write("--   with " + str(o) + " object(s) and \n") 
    f.write("\n ") 
    f.write("Semantics=SingleAssignment; \n ") 
    f.write("\n ") 
    f.write("Agent Environment\n")
    f.write("    Obsvars:\n")
    if a > 1:
        f.write("      turn  : 1.." + str(a) + ";\n") 
    for m in range(1, o+1):
        f.write("      obj" + str(m) + "x : 1.." + str(pow2) + "; \n")
        f.write("      obj" + str(m) + "y : {free," + ",".join(["hold" + str(n) for n in range(1, a+1)]) + "}; \n")
        
    for m in range(1, a+1):
        f.write("      posA" + str(m) + " : 1.." + str(pow2) + ";\n")
    f.write("    end Obsvars\n")
    f.write("    Actions = {none};\n")
    f.write("    Protocol:\n")
    f.write("      Other : {none};\n")
    f.write("    end Protocol\n")
    f.write("    Evolution:\n")
    if a > 1:
        f.write("      turn = turn + 1 if turn < " + str(a) + ";\n")
        f.write("      turn = 1 if turn = " + str(a) + ";\n")
    for m in range(1, a+1):
        for n in range(1, o+1):
            f.write("      obj" + str(n) + "y = hold" + str(m) + " if obj" + str(n) + "y = free and obj" + str(n) + "x = posA" + str(m) + " and A" + str(m) + ".Action = pickup;\n")
    for m in range(1, a+1):
        for n in range(1, o+1):
            f.write("      obj" + str(n) + "y = free  if obj" + str(n) + "y = hold" + str(m) + " and A" + str(m) + ".Action = putdown " + (" and " if o > 1 else "") + " and ".join(["(!(obj" + str(k) + "y = free) or !(obj" + str(k) + "x=posA" + str(m) + "))" for k in range(1, o+1) if k!=n])  + ";\n")
            f.write("      obj" + str(n) + "x = posA" + str(m) + " if obj" + str(n) + "y = hold" + str(m) + " and A" + str(m) + ".Action = putdown " + (" and " if o > 1 else "") + " and ".join(["(!(obj" + str(k) + "y = free) or !(obj" + str(k) + "x=posA" + str(m) + "))" for k in range(1, o+1) if k!=n])  + ";\n")
    
    for m in range(1, a+1):
        if m == 1:
            f.write("      posA1 = posA1 - 1 if A1.Action = left  and posA1 > 1 ;\n")
        else:
            f.write("      posA" + str(m) + " = posA" + str(m) + " - 1 if A" + str(m) + ".Action = left  and posA" + str(m) + " - 1 > posA" + str(m-1) + ";\n")
        if m < a:
            f.write("      posA" + str(m) + " = posA" + str(m) + " + 1 if A" + str(m) + ".Action = right and posA" + str(m) + " + 1 < posA" + str(m+1) + ";\n")
        else:
            f.write("      posA" + str(m) + " = posA" + str(m) + " + 1 if A" + str(m) + ".Action = right and posA" + str(m) + " < " + str(p) + " ;\n")
    f.write("    end Evolution\n")
    f.write("end Agent\n\n\n")
    for m in range(1, a+1):
        f.write("Agent A" + str(m) + "\n")
        f.write("    Vars:\n")
        f.write("        state: {none};\n")
        f.write("    end Vars\n")
        f.write("    Actions = {idle, left(2), right(2), pickup(3), putdown(1) };\n")
        f.write("    Protocol:\n")
        f.write("        Environment.posA" + str(m) + " >= 1 : {idle};\n")
        turn = "Environment.turn = " + str(m) if a > 1 else ""
        for n in range(1, o+1):
            f.write("        " + turn + (" and " if a > 1 else "") + "Environment.obj" + str(n) + "y = hold" + str(m) + (" and " if o > 1 else "") + " and ".join(["(!(Environment.obj" + str(k) + "y=free) or !(Environment.obj" + str(k) + "x = Environment.posA" + str(m) + "))" for k in range(1, o+1) if k!=n]) + " : {putdown};\n")
        for n in range(1, o+1):
            f.write("        " + turn + (" and " if a > 1 else "") + " and ".join(["!(Environment.obj" + str(k) + "y = hold" + str(m) + ")" for k in range(1,o+1) if k!=n]) + (" and " if o > 1 else "") + "Environment.obj" + str(n) + "x = Environment.posA" + str(m) + " and Environment.obj" + str(n) + "y = free : {pickup};\n")
        f.write("        " + turn + (" and " if a > 1 else "") + "Environment.posA" + str(m) + " < " + str(p) + " : {right};\n")
        f.write("        " + turn + (" and " if a > 1 else "") + "Environment.posA" + str(m) + " > 1 : {left};\n")
        f.write("    end Protocol\n")
        f.write("    Evolution:\n")
        f.write("      state=none if state = none;\n")
        f.write("    end Evolution\n")
        f.write("end Agent\n\n\n")

    f.write("Evaluation\n")
    f.write("    MoveDone if " + " and ".join(["Environment.obj" + str(m) + "x >= " + str(p-o+1) for m in range(1, o+1)]) + " and " + " and ".join(["Environment.obj" + str(m) + "y = free" for m in range(1, o + 1)]) + ";\n")
    if o > 1:
        f.write("    ObjsSamePlace if " + " or (".join([" and ".join(["Environment.obj" + str(m) + "x = Environment.obj" + str(n) + "x and Environment.obj" + str(m) + "y = free and Environment.obj" + str(n) + "y = free" for n in range(1, o+1) if m!=n]) for m in range(1,o+1)]) + ");\n")
    if a > 1 and o > 1:
        f.write("    AgentPickTwice if " + " or ".join([" or ".join([" and ".join(["(Environment.obj" + str(k) + "y = hold" + str(m) + " and Environment.obj" + str(n) + "y = hold" + str(m) + ")" for k in range(1, o+1) if k!=n]) for n in range(1, o+1)]) for m in range(1, a+1)]) + ";\n")
    if a > 1:
        f.write("    AgentSwapSide if " + " or ".join(["Environment.posA" + str(m) + " >= Environment.posA" + str(m+1)  for m in range(1, a)]) + ";\n")

    f.write("end Evaluation\n")

    f.write("InitStates\n")
    if a > 1:
        f.write("    Environment.turn = 1 and\n")
    f.write("    " + " and ".join(["Environment.obj" + str(m) + "x = " + str(m)  for m in range(1, o+1)]) + " and \n")
    f.write("    " + " and ".join(["Environment.obj" + str(m) + "y = free"       for m in range(1, o+1)]) + " and \n")
    f.write("    " + " and ".join(["Environment.posA" + str(m) + " = " + str(m if m <= a / 2 else p - (a - m))  for m in range(1, a+1)]) + ";\n")
    f.write("end InitStates\n")

    f.write("Groups\n")
    if a > 1:
        for m in range(1, a+1):
            # f.write("    gA" +str(m)+ " = {A" +str(m)+ "};\n")
            f.write("    gwoA" +str(m)+ " = {" + ",".join(["A" + str(n) for n in range(1, a+1) if n!=m]) + "};\n")
    f.write("    gAll = {" + ",".join(["A" + str(m) for m in range(1, a+1)]) + "};\n")
    f.write("end Groups\n")

    f.write("Formulae\n")
    if o > 1:
        f.write("    AG !ObjsSamePlace;\n")
    if a > 1 and o > 1:
        f.write("    AG !AgentPickTwice;\n")
    if a > 1:
        f.write("    AG !AgentSwapSide;\n")
    f.write("    EF MoveDone;\n")
    if a > 1:
        for m in range(1, a+1):
            f.write("    <gwoA" + str(m) + ">F MoveDone;\n")
    for m in range(1, 41):
        f.write("    <gAll>(" + str(m) + ")F MoveDone;\n")
    f.write("end Formulae\n")


if __name__ == "__main__":
    args = sys.argv;
    try:
        nOfPos    = int(args[1])
        nOfAgents = int(args[2])
        nOfObjs   = int(args[3])
        # condition: 
        # nOfPos >= 2
        # nOfAgents >= 1 and <= nOfPos - 1
        # nOfObjs >=1 and <= floor(nOfPos / 2)
        if (nOfPos < 2):
            raise
        if (nOfAgents < 1 or nOfAgents > nOfPos - 1):
            raise
        if (nOfObjs < 1 or nOfObjs > nOfPos / 1):
            raise
        GenATB(nOfPos, nOfAgents, nOfObjs);
    except:
        print("""Generate Model for the Race Car example.
Usage:
     genRace n a o
     where 
        n : number of positions
        a : number of agents
        o : number of objects
    and
        n >= 2
        n - 1 >= a >= 1
        n / 2 >= o >= 1  
              """)
    
