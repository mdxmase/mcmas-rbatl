import sys
import math

def GenATB(p, a, o):
    pow2 = int(2**math.ceil(math.log(p,2)))
    fc = ("-- encoding example \"ArmTowerBlock\" from Clark's presentation (Teleo-Reactive)\n")
    fc = fc + ("--   with " + str(p) + " positions \n") 
    fc = fc + ("--   with " + str(a) + " agent(s) and \n") 
    fc = fc + ("--   with " + str(o) + " object(s) and \n") 
    fc = fc + ("\n ") 
    fc = fc + ("Semantics=SingleAssignment; \n ") 
    fc = fc + ("\n ") 
    fc = fc + ("Agent Environment\n")
    fc = fc + ("    Obsvars:\n")
    if a > 1:
        fc = fc + ("      turn  : 1.." + str(a) + ";\n") 
    for m in range(1, o+1):
        fc = fc + ("      obj" + str(m) + "x : 1.." + str(pow2) + "; \n")
        fc = fc + ("      obj" + str(m) + "y : {free," + ",".join(["hold" + str(n) for n in range(1, a+1)]) + "}; \n")
        
    for m in range(1, a+1):
        fc = fc + ("      posA" + str(m) + " : 1.." + str(pow2) + ";\n")
    fc = fc + ("    end Obsvars\n")
    fc = fc + ("    Actions = {none};\n")
    fc = fc + ("    Protocol:\n")
    fc = fc + ("      Other : {none};\n")
    fc = fc + ("    end Protocol\n")
    fc = fc + ("    Evolution:\n")
    if a > 1:
        fc = fc + ("      turn = turn + 1 if turn < " + str(a) + ";\n")
        fc = fc + ("      turn = 1 if turn = " + str(a) + ";\n")
    for m in range(1, a+1):
        for n in range(1, o+1):
            fc = fc + ("      obj" + str(n) + "y = hold" + str(m) + " if obj" + str(n) + "y = free and obj" + str(n) + "x = posA" + str(m) + " and A" + str(m) + ".Action = pickup;\n")
    for m in range(1, a+1):
        for n in range(1, o+1):
            fc = fc + ("      obj" + str(n) + "y = free  if obj" + str(n) + "y = hold" + str(m) + " and A" + str(m) + ".Action = putdown " + (" and " if o > 1 else "") + " and ".join(["(!(obj" + str(k) + "y = free) or !(obj" + str(k) + "x=posA" + str(m) + "))" for k in range(1, o+1) if k!=n])  + ";\n")
            fc = fc + ("      obj" + str(n) + "x = posA" + str(m) + " if obj" + str(n) + "y = hold" + str(m) + " and A" + str(m) + ".Action = putdown " + (" and " if o > 1 else "") + " and ".join(["(!(obj" + str(k) + "y = free) or !(obj" + str(k) + "x=posA" + str(m) + "))" for k in range(1, o+1) if k!=n])  + ";\n")
    
    for m in range(1, a+1):
        if m == 1:
            fc = fc + ("      posA1 = posA1 - 1 if A1.Action = left  and posA1 > 1 ;\n")
        else:
            fc = fc + ("      posA" + str(m) + " = posA" + str(m) + " - 1 if A" + str(m) + ".Action = left  and posA" + str(m) + " - 1 > posA" + str(m-1) + ";\n")
        if m < a:
            fc = fc + ("      posA" + str(m) + " = posA" + str(m) + " + 1 if A" + str(m) + ".Action = right and posA" + str(m) + " + 1 < posA" + str(m+1) + ";\n")
        else:
            fc = fc + ("      posA" + str(m) + " = posA" + str(m) + " + 1 if A" + str(m) + ".Action = right and posA" + str(m) + " < " + str(p) + " ;\n")
    fc = fc + ("    end Evolution\n")
    fc = fc + ("end Agent\n\n\n")
    for m in range(1, a+1):
        fc = fc + ("Agent A" + str(m) + "\n")
        fc = fc + ("    Vars:\n")
        fc = fc + ("        state: {none};\n")
        fc = fc + ("    end Vars\n")
        fc = fc + ("    Actions = {idle, left(2,1), right(2,1), pickup(3,2), putdown(1,3) };\n")
        fc = fc + ("    Protocol:\n")
        fc = fc + ("        Environment.posA" + str(m) + " >= 1 : {idle};\n")
        turn = "Environment.turn = " + str(m) if a > 1 else ""
        for n in range(1, o+1):
            fc = fc + ("        " + turn + (" and " if a > 1 else "") + "Environment.obj" + str(n) + "y = hold" + str(m) + (" and " if o > 1 else "") + " and ".join(["(!(Environment.obj" + str(k) + "y=free) or !(Environment.obj" + str(k) + "x = Environment.posA" + str(m) + "))" for k in range(1, o+1) if k!=n]) + " : {putdown};\n")
        for n in range(1, o+1):
            fc = fc + ("        " + turn + (" and " if a > 1 else "") + " and ".join(["!(Environment.obj" + str(k) + "y = hold" + str(m) + ")" for k in range(1,o+1) if k!=n]) + (" and " if o > 1 else "") + "Environment.obj" + str(n) + "x = Environment.posA" + str(m) + " and Environment.obj" + str(n) + "y = free : {pickup};\n")
        fc = fc + ("        " + turn + (" and " if a > 1 else "") + "Environment.posA" + str(m) + " < " + str(p) + " : {right};\n")
        fc = fc + ("        " + turn + (" and " if a > 1 else "") + "Environment.posA" + str(m) + " > 1 : {left};\n")
        fc = fc + ("    end Protocol\n")
        fc = fc + ("    Evolution:\n")
        fc = fc + ("      state=none if state = none;\n")
        fc = fc + ("    end Evolution\n")
        fc = fc + ("end Agent\n\n\n")

    fc = fc + ("Evaluation\n")
    fc = fc + ("    MoveDone if " + " and ".join(["Environment.obj" + str(m) + "x >= " + str(p-o+1) for m in range(1, o+1)]) + " and " + " and ".join(["Environment.obj" + str(m) + "y = free" for m in range(1, o + 1)]) + ";\n")
    if o > 1:
        fc = fc + ("    ObjsSamePlace if " + " or (".join([" and ".join(["Environment.obj" + str(m) + "x = Environment.obj" + str(n) + "x and Environment.obj" + str(m) + "y = free and Environment.obj" + str(n) + "y = free" for n in range(1, o+1) if m!=n]) for m in range(1,o+1)]) + ");\n")
    if a > 1 and o > 1:
        fc = fc + ("    AgentPickTwice if " + " or ".join([" or ".join([" and ".join(["(Environment.obj" + str(k) + "y = hold" + str(m) + " and Environment.obj" + str(n) + "y = hold" + str(m) + ")" for k in range(1, o+1) if k!=n]) for n in range(1, o+1)]) for m in range(1, a+1)]) + ";\n")
    if a > 1:
        fc = fc + ("    AgentSwapSide if " + " or ".join(["Environment.posA" + str(m) + " >= Environment.posA" + str(m+1)  for m in range(1, a)]) + ";\n")

    fc = fc + ("end Evaluation\n")

    fc = fc + ("InitStates\n")
    if a > 1:
        fc = fc + ("    Environment.turn = 1 and\n")
    fc = fc + ("    " + " and ".join(["Environment.obj" + str(m) + "x = " + str(m)  for m in range(1, o+1)]) + " and \n")
    fc = fc + ("    " + " and ".join(["Environment.obj" + str(m) + "y = free"       for m in range(1, o+1)]) + " and \n")
    fc = fc + ("    " + " and ".join(["Environment.posA" + str(m) + " = " + str(m if m <= a / 2 else p - (a - m))  for m in range(1, a+1)]) + ";\n")
    fc = fc + ("end InitStates\n")

    fc = fc + ("Groups\n")
    if a > 1:
        for m in range(1, a+1):
            # fc = fc + ("    gA" +str(m)+ " = {A" +str(m)+ "};\n")
            fc = fc + ("    gwoA" +str(m)+ " = {" + ",".join(["A" + str(n) for n in range(1, a+1) if n!=m]) + "};\n")
    fc = fc + ("    gAll = {" + ",".join(["A" + str(m) for m in range(1, a+1)]) + "};\n")
    fc = fc + ("end Groups\n")

    fc = fc + ("Formulae\n")
    # if o > 1:
        # fc = fc + ("    AG !ObjsSamePlace;\n")
    # if a > 1 and o > 1:
        # fc = fc + ("    AG !AgentPickTwice;\n")
    # if a > 1:
        # fc = fc + ("    AG !AgentSwapSide;\n")
    # fc = fc + ("    EF MoveDone;\n")
    # if a > 1:
        # for m in range(1, a+1):
            # fc = fc + ("    <gwoA" + str(m) + ">F MoveDone;\n")
    # for m in range(1, 41):
        # fc = fc + ("    <gAll>(" + str(m) + "," + str(m) + ")F MoveDone;\n")

    fc0 = fc + ("    <gAll>F MoveDone;\n") + ("end Formulae\n")
    fc1 = fc + ("    <gAll>(8,7)F MoveDone;\n") + ("end Formulae\n")
    fc2 = fc + ("    <gAll>(12,18)F MoveDone;\n") + ("end Formulae\n")
    fc3 = fc + ("    <gAll>(24,24)F MoveDone;\n") + ("end Formulae\n")

    f1 = open("ArmTowerBlock2res0bound-" + str(p) + "-" + str(a) + "-" + str(o) + ".ispl","w")
    f1.write(fc0)
    f1.close()
    # f1 = open("ArmTowerBlock2res1stform-" + str(p) + "-" + str(a) + "-" + str(o) + ".ispl","w")
    # f1.write(fc1)
    # f1.close()

    # f2 = open("ArmTowerBlock2res2ndform-" + str(p) + "-" + str(a) + "-" + str(o) + ".ispl","w")
    # f2.write(fc2)
    # f2.close()

    # f3 = open("ArmTowerBlock2res3rdform-" + str(p) + "-" + str(a) + "-" + str(o) + ".ispl","w")
    # f3.write(fc3)
    # f3.close()

if __name__ == "__main__":
    args = sys.argv;
    try:
        nOfPos    = int(args[1])
        nOfAgents = int(args[2])
        nOfObjs   = int(args[3])
        # condition: 
        # nOfPos >= 2
        # nOfAgents >= 1 and <= nOfPos - 1
        # nOfObjs >=1 and <= floor(nOfPos / 2)
        if (nOfPos < 2):
            raise
        if (nOfAgents < 1 or nOfAgents > nOfPos - 1):
            raise
        if (nOfObjs < 1 or nOfObjs > nOfPos / 1):
            raise
        GenATB(nOfPos, nOfAgents, nOfObjs);
    except:
        print("""Generate Model for the Race Car example.
Usage:
     genArmTowerBlock2res n a o
     where 
        n : number of positions
        a : number of agents
        o : number of objects
    and
        n >= 2
        n - 1 >= a >= 1
        n / 2 >= o >= 1  
              """)
    
