# Takes 3 parameters:
# N = width of the tree
# D = depth of the tree
# M = number of nodes in cluster

import sys

# Some default parameters
MAX_CH_ITERATIONS = 11;
SEND_COST = 1;
RECEIVE_COST = 1;

# the list of actions for the base station (populated below)

bsActions = []

def GenLeach(n,d,m):

    f = open("leach-" + str(n) + "-" + str(d) + "-" + str(m) + ".ispl","w");

    # Total number of agents:
    # nAgents = 1+ n*m + n*n*m + n*n*n*m + .. = 1 + m * n * (1 + n + n^2 +...+n^(d-1)
    # 
    nAgents = 1
    for i in range(1,(d+1)):
        nAgents += (n**i)*m

    # Total number of clusters: nClusters = n+n^2+..+n^d
    nClusters = 0
    for i in range(1,(d+1)):
        nClusters += (n**i)
    
    f.write("-- LEACH protocol, width = " + str(n) +"\n")
    f.write("-- Total number of agents = " + str(nAgents) + "\n")
    f.write("-- Total number of clusters = " + str(nClusters) + "\n")

    f.write("Semantics=SingleAssignment;\n");

    f.write("\n") 

    f.write("Agent BaseStation\n")
    f.write("  Vars:\n")

    # it works because it iterates to nAgents-1 
    for i in range(1,nAgents):
        f.write("    A"+str(i)+"_finished : boolean;\n")
    f.write("  end Vars\n")

    f.write("  Actions = { \n")
    # There are m different actions to set a CH, each action sets n clusters
    for i in range(1,(m+1)):
        f.write("              set")
        curAction = "set"
        for j in range(0,nClusters):
            f.write("_A"+str(i+(m*j)))
            curAction += "_A"+str(i+(m*j))
        f.write("_CH,\n")
        curAction += "_CH"
        bsActions.append(curAction)

    f.write("              send_terminate, nothing };\n")

    f.write("  Protocol:\n");
    # iterate over all the agents that could finish (minus one):
    for i in range (1,m):
        for j in range(0,nClusters):
            for k in range (1,(m+1)):

                if ( k <= i ):
                    f.write("(A"+str(k+j*m)+"_finished = true)")
                else:
                    f.write("(A"+str(k+j*m)+"_finished = false)")

                if ( k != m ):
                    f.write(" and ")
                elif ( j != (nClusters-1)):
                    f.write(" and\n")
                else: 
                    f.write(" : ")



        f.write(" {set")
        for k in range(0,nClusters):
            f.write("_A"+str(1+i+(m*k)))

        f.write("_CH};\n")

    for i in range (0,nClusters):
        f.write("(A"+str(m+m*i)+"_finished = true)")
        if ( i != (nClusters-1) ):
            f.write(" and ")

    f.write(" : { send_terminate };\n")
    f.write("    Other : {nothing};\n")
    f.write("  end Protocol")
    f.write("  Evolution:\n")
    for i in range(1,nAgents): 
        f.write("    (A"+str(i)+"_finished=true) if (A"+str(i)+".Action=invoke_election);\n")
    f.write("  end Evolution\n")
    f.write("end Agent\n")
    f.write("\n-- End of BaseStation --\n\n")



    # Just a counter for the current agent
    agentCounter = 1

    # We iterate over depth of tree
    for curDepth in range (1,(d+1)):
        f.write("----- BEGIN CLUSTERS at depth "+str(curDepth)+"-----\n")

        # At depth d we have n^d clusters
        for curCluster in range(0,(n**curDepth)):

            # And in each cluster we have m agents:
            for curAgent in range (0,m):
                f.write("Agent A"+str(agentCounter)+"\n")
                f.write("  Vars:\n")
                f.write("    type : {CH, SN};\n")
                f.write("    status : {main_loop, election, finished};\n")
                f.write("    curround : 0.."+str(MAX_CH_ITERATIONS)+";\n")
                f.write("    was_CH : boolean;\n")
                f.write("  end Vars\n");

                ch_cost = RECEIVE_COST*(m-1) + SEND_COST

                if (curDepth != d):
                    ch_cost += m*RECEIVE_COST
                
                f.write("  Actions = { send("+str(SEND_COST)+"), \n")
                f.write("              ch_receive_and_send("+str(ch_cost)+"),\n")
                f.write("              sleep, invoke_election };\n")
                f.write("  Protocol:")
                f.write("    (status=election) : {sleep};\n")
                f.write("    (status=finished) : {sleep};\n")
                f.write("    (status=main_loop) and (curround<"+str(MAX_CH_ITERATIONS-1)+")"+
                         " and (type=SN) : {send};\n")
                f.write("    (status=main_loop) and (curround<"+str(MAX_CH_ITERATIONS-1)+")"+
                         " and (type=CH) : {ch_receive_and_send};\n")
                f.write("    (curround="+str(MAX_CH_ITERATIONS-1)+") and (type=CH) : {invoke_election};\n")
                f.write("    (curround="+str(MAX_CH_ITERATIONS-1)+") and (type=SN) : {sleep};\n")
                f.write("    (curround>"+str(MAX_CH_ITERATIONS-1)+") : {sleep};\n")
                f.write("  end Protocol\n")

                f.write("  Evolution:\n")

                # The action of the BS containing this agent as target of a set CH
                f.write("    (type=CH) if BaseStation.Action=" + 
                        filter(lambda x: ("_A"+str(agentCounter)+"_") in x, bsActions).pop()
                        + " and (was_CH=false);\n")

                # The actions of the BS *not* containing this agent as a target of a set CH
                f.write("    (type=SN) if (" + " or ".join(
                    map(lambda x: "BaseStation.Action="+x,
                        filter(lambda y: y.find("_A"+str(agentCounter)+"_") < 0, bsActions)
                        )
                    )
                    + ");\n")

                # As above
                f.write("    (was_CH=true) if (was_CH=false) and BaseStation.Action=" + 
                        filter(lambda x: ("A"+str(agentCounter)+"_") in x, bsActions).pop() + ";\n")

                f.write("    (status=election) if (Action=invoke_election) ")

                # How many agents before this depth?
                prevAgents = 1;
                for i in range(1,curDepth):
                    prevAgents += (n**i)*m
                # All the agents in this cluster. curDepth from 1 and curCluster from 0
                prevAgents += (curCluster*m)
                for i in range(prevAgents,(prevAgents+m)):
                    if (i != agentCounter):
                        f.write(" or (A"+str(i)+".Action=invoke_election)")

                f.write(";\n")


                f.write("    curround = curround + 1 if (curround <"+str(MAX_CH_ITERATIONS-1)+") and (status=main_loop);\n")
                f.write("    curround = 0 if (curround="+str(MAX_CH_ITERATIONS-1)+");\n")
                f.write("    (status = main_loop) if (curround=0) and !(BaseStation.Action=send_terminate);\n")
                f.write("    (status=finished) if (BaseStation.Action=send_terminate);\n")
                f.write("  end Evolution\n")
                f.write("end Agent\n\n")
                
                agentCounter += 1
        
        f.write("----- END CLUSTERS at depth "+str(curDepth)+"-----\n")

    LastLines = """ 

Evaluation
    Finished if (A1.status=finished);
end Evaluation


"""
    f.write(LastLines)

    f.write("InitStates\n")
    f.write("  BaseStation.A1_finished=false")
    
    for i in range(2,nAgents):
        f.write("  and BaseStation.A"+str(i)+"_finished=false")
        if ( (i%2) == 0):
            f.write("\n")
    f.write("\n\n")

    f.write("  and A1.type=CH")
    for i in range(2,nAgents):
        if ( ((i-1)%m) == 0 ):
            f.write("  and A"+str(i)+".type=CH")
        else:
            f.write("  and A"+str(i)+".type=SN")
        if ( (i%m) == 0):
            f.write("\n")
    f.write("\n")

    f.write("  and A1.was_CH=true")
    for i in range(2,nAgents):
        if ( ((i-1)%m) == 0 ):
            f.write("  and A"+str(i)+".was_CH=true")
        else:
            f.write("  and A"+str(i)+".was_CH=false")
        if ( (i%m) == 0):
            f.write("\n")
    f.write("\n")

    f.write("  and A1.status=main_loop")    
    for i in range(2,nAgents):
        f.write("  and A"+str(i)+".status=main_loop")
        if ( (i%3) == 0):
            f.write("\n")
    f.write("\n\n")

    f.write("  and A1.curround=0")    
    for i in range(2,nAgents):
        f.write("  and A"+str(i)+".curround=0")
        if ( (i%3) == 0):
            f.write("\n")
    f.write("  ;\n")
    f.write("end InitStates\n\n")


    # One group for each agent
    f.write("Groups\n")
    for i in range(1,nAgents):
        f.write("  gA"+str(i)+"={A"+str(i)+"};\n")
    f.write("end Groups\n\n")

    f.write("Formulae\n")
    f.write("  <gA1>(10)F Finished;\n")
    f.write("  <gA1>(50)F Finished;\n")
    f.write("  <gA1>(100)F Finished;\n")
    f.write("end Formulae")
    f.close()

    print(bsActions)

if __name__ == "__main__":
    args = sys.argv;
    try:
        n = int(args[1])
        d = int(args[2])
        m = int(args[3])
        if ((n < 1) or (m < 1) or (d < 1)):
            raise
        GenLeach(n,d,m);
    except:
        print("""Generate Model for an instance of LEACH.
Usage:
     genLeach n d m
     n,d,m >= 1
              """)
    
