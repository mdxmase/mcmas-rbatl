import sys

def GenRace(h,l):
    iState = "S0";
    hNum = 3*h + 1;
    lNum = l + 1;
    hStates = { k : "H" + str(k) for k in range(1,hNum+1) };
    lStates = { k : "L" + str(k) for k in range(1,lNum+1) };

    f = open("race-" + str(h) + "-" + str(l) + ".ispl","w");
    
    f.write("-- encoding for the race car example in revision 82 of the ECAI submission\n")
    f.write("--   with " + str(h) + " high league rounds and\n") 
    f.write("--   with " + str(l) + " low league rounds.") 
    f.write("\n") 
    f.write("Agent Environment\n")
    f.write("    Obsvars:\n")
    f.write("      state : {")
    f.write(iState)
    f.write(",")
    f.write(",".join([hStates[k] for k in range(1,hNum+1)]))
    f.write(",")
    f.write(",".join([lStates[k] for k in range(1,lNum+1)]))
    f.write("};\n")
    f.write("    end Obsvars\n")
    f.write("    Actions = {idle, w, n};\n")
    f.write("    Protocol:\n")
    f.write("      " + " or ".join(["state = " + hStates[k] for k in range(1, hNum, 3)]) + " : {w, n};\n")
    f.write("      Other : {idle};\n")
    f.write("    end Protocol\n")
    f.write("    Evolution:\n")
    f.write("        state=H1  if state = S0 and Car.Action = h;\n")
    f.write("        state=L1  if state = S0 and Car.Action = l;\n")
    for k in range(1,hNum,3):
        f.write("        state=" + hStates[k+1] + "  if state = " + hStates[k] + " and Action = w;\n")
        f.write("        state=" + hStates[k+2] + "  if state = " + hStates[k] + " and Action = n;\n")
    for k in range(1,hNum,3):
        f.write("        state=" + hStates[k+3] + "  if state = " + hStates[k+1] + ";\n")
        f.write("        state=" + hStates[k+3] + "  if state = " + hStates[k+2] + ";\n")
    f.write("        state=S0  if state = " + hStates[hNum] + ";\n")
    for k in range(1, lNum):
        f.write("        state = " + lStates[k+1] + "  if state = " + lStates[k] + ";\n")
    f.write("        state=S0  if state = " + lStates[lNum] + ";\n")
    f.write("    end Evolution\n")
    f.write("end Agent\n\n\n")
    f.write("Agent Car\n")
    f.write("    Vars:\n")
    f.write("        state: {none};\n")
    f.write("    end Vars\n")
    f.write("    Actions = {idle, a(-2), b(2), c(-1), l, h };\n")
    f.write("    Protocol:\n")
    f.write("        Environment.state=S0 : {l, h};\n")
    f.write("        " + " or ".join(["Environment.state = " + hStates[k] for k in range(2,hNum,3)]) + " : {a};\n")
    f.write("        " + " or ".join(["Environment.state = " + hStates[k] for k in range(3,hNum,3)]) + " : {b};\n")
    f.write("        " + " or ".join(["Environment.state = " + lStates[k] for k in range(1,lNum)]) + " : {c};\n")
    f.write("      Other: {idle};\n")
    f.write("    end Protocol\n")
    f.write("    Evolution:\n")
    f.write("      state=none if state = none;\n")
    f.write("    end Evolution\n")
    f.write("end Agent\n\n\n")
    f.write("Evaluation\n")
    f.write("    CompleteH if Environment.state = " + hStates[hNum] + ";\n")
    f.write("end Evaluation\n")
    LastLines = """ 
InitStates
    Environment.state=S0;
end InitStates
 
Groups
    gCar={Car};
end Groups
 
Formulae
    <gCar>(0) F CompleteH;
end Formulae
    """
    f.write(LastLines)
    f.close()


if __name__ == "__main__":
    args = sys.argv;
    try:
        n = int(args[1])
        m = int(args[2])
        if (n < 1 or m < 1):
            raise
        GenRace(n,m);
    except:
        print("""Generate Model for the Race Car example.
Usage:
     genRace n m
     n,m >= 1
              """)
    
