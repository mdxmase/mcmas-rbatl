#!/bin/bash

./mcmas examples/scripts/ArmTowerBlock2rfes-2-1-1.ispl > log2res1stform-2-1-1.txt
./mcmas examples/scripts/ArmTowerBlock2res1stform-3-1-1.ispl > log2res1stform-3-1-1.txt
./mcmas examples/scripts/ArmTowerBlock2res1stform-4-1-1.ispl > log2res1stform-4-1-1.txt
./mcmas examples/scripts/ArmTowerBlock2res1stform-4-1-2.ispl > log2res1stform-4-1-2.txt
./mcmas examples/scripts/ArmTowerBlock2res1stform-3-2-1.ispl > log2res1stform-3-2-1.txt
./mcmas examples/scripts/ArmTowerBlock2res1stform-4-2-1.ispl > log2res1stform-4-2-1.txt
./mcmas examples/scripts/ArmTowerBlock2res1stform-4-2-2.ispl > log2res1stform-4-2-2.txt
./mcmas -explicitpre examples/scripts/ArmTowerBlock2res1stform-2-1-1.ispl > log2res1stform-expre-2-1-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res1stform-3-1-1.ispl > log2res1stform-expre-3-1-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res1stform-4-1-1.ispl > log2res1stform-expre-4-1-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res1stform-4-1-2.ispl > log2res1stform-expre-4-1-2.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res1stform-3-2-1.ispl > log2res1stform-expre-3-2-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res1stform-4-2-1.ispl > log2res1stform-expre-4-2-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res1stform-4-2-2.ispl > log2res1stform-expre-4-2-2.txt

./mcmas examples/scripts/ArmTowerBlock2res2ndform-2-1-1.ispl > log2res2ndform-2-1-1.txt
./mcmas examples/scripts/ArmTowerBlock2res2ndform-3-1-1.ispl > log2res2ndform-3-1-1.txt
./mcmas examples/scripts/ArmTowerBlock2res2ndform-4-1-1.ispl > log2res2ndform-4-1-1.txt
./mcmas examples/scripts/ArmTowerBlock2res2ndform-4-1-2.ispl > log2res2ndform-4-1-2.txt
./mcmas examples/scripts/ArmTowerBlock2res2ndform-3-2-1.ispl > log2res2ndform-3-2-1.txt
./mcmas examples/scripts/ArmTowerBlock2res2ndform-4-2-1.ispl > log2res2ndform-4-2-1.txt
./mcmas examples/scripts/ArmTowerBlock2res2ndform-4-2-2.ispl > log2res2ndform-4-2-2.txt
./mcmas -explicitpre examples/scripts/ArmTowerBlock2res2ndform-2-1-1.ispl > log2res2ndform-expre-2-1-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res2ndform-3-1-1.ispl > log2res2ndform-expre-3-1-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res2ndform-4-1-1.ispl > log2res2ndform-expre-4-1-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res2ndform-4-1-2.ispl > log2res2ndform-expre-4-1-2.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res2ndform-3-2-1.ispl > log2res2ndform-expre-3-2-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res2ndform-4-2-1.ispl > log2res2ndform-expre-4-2-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res2ndform-4-2-2.ispl > log2res2ndform-expre-4-2-2.txt

./mcmas examples/scripts/ArmTowerBlock2res3rdform-2-1-1.ispl > log2res3rdform-2-1-1.txt
./mcmas examples/scripts/ArmTowerBlock2res3rdform-3-1-1.ispl > log2res3rdform-3-1-1.txt
./mcmas examples/scripts/ArmTowerBlock2res3rdform-4-1-1.ispl > log2res3rdform-4-1-1.txt
./mcmas examples/scripts/ArmTowerBlock2res3rdform-4-1-2.ispl > log2res3rdform-4-1-2.txt
./mcmas examples/scripts/ArmTowerBlock2res3rdform-3-2-1.ispl > log2res3rdform-3-2-1.txt
./mcmas examples/scripts/ArmTowerBlock2res3rdform-4-2-1.ispl > log2res3rdform-4-2-1.txt
./mcmas examples/scripts/ArmTowerBlock2res3rdform-4-2-2.ispl > log2res3rdform-4-2-2.txt
./mcmas -explicitpre examples/scripts/ArmTowerBlock2res3rdform-2-1-1.ispl > log2res3rdform-expre-2-1-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res3rdform-3-1-1.ispl > log2res3rdform-expre-3-1-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res3rdform-4-1-1.ispl > log2res3rdform-expre-4-1-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res3rdform-4-1-2.ispl > log2res3rdform-expre-4-1-2.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res3rdform-3-2-1.ispl > log2res3rdform-expre-3-2-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res3rdform-4-2-1.ispl > log2res3rdform-expre-4-2-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock2res3rdform-4-2-2.ispl > log2res3rdform-expre-4-2-2.txt
