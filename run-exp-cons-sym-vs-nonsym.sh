#!/bin/bash

./mcmas examples/scripts/ArmTowerBlock-2-1-1.ispl > log-2-1-1.txt
./mcmas examples/scripts/ArmTowerBlock-3-1-1.ispl > log-3-1-1.txt
./mcmas examples/scripts/ArmTowerBlock-4-1-1.ispl > log-4-1-1.txt
./mcmas examples/scripts/ArmTowerBlock-4-1-2.ispl > log-4-1-2.txt
./mcmas examples/scripts/ArmTowerBlock-3-2-1.ispl > log-3-2-1.txt
./mcmas examples/scripts/ArmTowerBlock-4-2-1.ispl > log-4-2-1.txt
./mcmas examples/scripts/ArmTowerBlock-4-2-2.ispl > log-4-2-2.txt
./mcmas -explicitpre examples/scripts/ArmTowerBlock-2-1-1.ispl > log-expre-2-1-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock-3-1-1.ispl > log-expre-3-1-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock-4-1-1.ispl > log-expre-4-1-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock-4-1-2.ispl > log-expre-4-1-2.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock-3-2-1.ispl > log-expre-3-2-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock-4-2-1.ispl > log-expre-4-2-1.txt
./mcmas -explicitpre  examples/scripts/ArmTowerBlock-4-2-2.ispl > log-expre-4-2-2.txt
