#!/bin/bash

./mcmas examples/scripts/ArmTowerBlock-2-1-1.ispl > log-2-1-1.txt
./mcmas examples/scripts/ArmTowerBlock-3-1-1.ispl > log-3-1-1.txt
./mcmas examples/scripts/ArmTowerBlock-4-1-1.ispl > log-4-1-1.txt
./mcmas examples/scripts/ArmTowerBlock-4-1-2.ispl > log-4-1-2.txt
./mcmas examples/scripts/ArmTowerBlock-3-2-1.ispl > log-3-2-1.txt
./mcmas examples/scripts/ArmTowerBlock-4-2-1.ispl > log-4-2-1.txt
./mcmas examples/scripts/ArmTowerBlock-4-2-2.ispl > log-4-2-2.txt
./mcmas -ignoreconsumption examples/scripts/ArmTowerBlock-2-1-1.ispl > log-ic-2-1-1.txt
./mcmas -ignoreconsumption  examples/scripts/ArmTowerBlock-3-1-1.ispl > log-ic-3-1-1.txt
./mcmas -ignoreconsumption  examples/scripts/ArmTowerBlock-4-1-1.ispl > log-ic-4-1-1.txt
./mcmas -ignoreconsumption  examples/scripts/ArmTowerBlock-4-1-2.ispl > log-ic-4-1-2.txt
./mcmas -ignoreconsumption  examples/scripts/ArmTowerBlock-3-2-1.ispl > log-ic-3-2-1.txt
./mcmas -ignoreconsumption  examples/scripts/ArmTowerBlock-4-2-1.ispl > log-ic-4-2-1.txt
./mcmas -ignoreconsumption  examples/scripts/ArmTowerBlock-4-2-2.ispl > log-ic-4-2-2.txt
./mcmas -explicitrbatl examples/scripts/ArmTowerBlock-2-1-1.ispl > log-ex-2-1-1.txt
./mcmas -explicitrbatl examples/scripts/ArmTowerBlock-3-1-1.ispl > log-ex-3-1-1.txt
./mcmas -explicitrbatl examples/scripts/ArmTowerBlock-4-1-1.ispl > log-ex-4-1-1.txt
./mcmas -explicitrbatl examples/scripts/ArmTowerBlock-4-1-2.ispl > log-ex-4-1-2.txt
./mcmas -explicitrbatl examples/scripts/ArmTowerBlock-3-2-1.ispl > log-ex-3-2-1.txt
./mcmas -explicitrbatl examples/scripts/ArmTowerBlock-4-2-1.ispl > log-ex-4-2-1.txt
./mcmas -explicitrbatl examples/scripts/ArmTowerBlock-4-2-2.ispl > log-ex-4-2-2.txt
